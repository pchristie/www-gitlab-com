---
layout: job_family_page
title: "Global Education Services"
--- 

## Levels

### Manager of Education Curriculum Development

The Manager of Education Curriculum Development reports into the Senior Director, Global Education.

#### Manager of Education Curriculum Development Job Grade 

The Manager of Education Curriculum Development is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager of Education Curriculum Development Responsibilities
* Manage a high performing direct team of instructional designers, multimedia developers and lab managers for DevOps and product content and course development to ensure effective education delivery
* Defines strategy and delivery of engaging GitLab and DevOps educational content for self-paced and instructor-led curriculum
* Extends the Senior Technical Instructional Designer responsibilities
* Proactively conducts and refreshes job task analysis and related documentation to ensure training develops the end user's skills and knowledge.
* Proactively iterates and creates solutions and processes to optimize instructional design workflow
* Defines, documents, and creates standards, templates, and processes to guide the work of instructional designer and curriculum development team members
* Develops and tracks attainment of KPIs and proactively leads a team-based approach to delivering to KPIs
* Regularly shares best practices and provides constructive coaching and feedback across the organization, both within and outside of the immediate team
* Promotes collaboration, partnership, and relationships among the learning & development and enablement ecosystem within GitLab, including Product, Learning and Development, Sales Enablement, Customer Success, Channel, and Marketing teams.
* Identifies and communicates opportunities for process and quality improvements, providing feedback on usage, risks, and suggested enhancements as a LXP SME
* Apply adult learning theory and instructional design, eLearning, certifications, and creation of knowledge transfer curriculum and taxonomy
* Collaborate with SMEs to design effective learning experiences, managing and supporting them through the course and certification development processes
* Content management of GitLab product education curriculum and learning offerings on the LXP
* Serves as an internal expert on instructional design methodologies, continuously monitoring latest innovations in the field and the performance of new products while sharing learnings with cross-functional GitLab team members
* Partner effectively with cross-functional curriculum developers, instructional designers, technical trainers, training managers, project managers, and internal stakeholders across the organization to ensure content aligns with GitLab instructional design best practices org wide


#### Manager of Education Curriculum Development Requirements

* Extends the Senior Technical Instructional Designer requirements
* 7+ years experience in an instructional designer role, preferably with expertise in DevOps and/or Open Source
* 2+ years of experience leading a team of education instruction designers and/or
* Strong project management, leadership, change management, and cross-functional collaboration skills with a track record of managing complex learning projects
* Experience collaborating across multiple stakeholder groups to deliver global solutions

### Senior Manager, Education Delivery

The Senior Manager, Professional Education Services reports to the Senior Director, Global Education.

#### Senior Manager, Education Delivery Job Grade

The Senior Manager, Education Delivery is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Manager, Education Delivery Responsibilities

* Set strategic direction for Professional Services Education Delivery
* Develop and execute inbound and outbound marketing strategy to align with business goals
* Manage execution and delivery of education services 
* Learning platform administration and reporting
* Clarify service offering details with the GitLab Field Sales team and Engagement Managers 
* Work with GitLab Customer Success and Sales team members to position, scope, and sell services 
* Define requirements for authoring tools and learning administration platform(s) 
* Plan and execute GitLab Services Partner onboarding and enablement
* Plan and operationalize internal Professional Services enablement working with team department leaders
* Team and talent development (hiring, performance management, growth and career development)
* Scale Education Services delivery through GitLab Partners
* Plan and execute of the GitLab Certified Training Partner program
* Oversee resource capacity, project delivery quality and operational success metrics including bookings, revenue, utilization, on-time completion, time to value (10% of sold users are active), CSAT, stage adoption, user adoption (growth), and renewals.
* Meet Education Services bookings and revenue goals
* Manage collaboration with stakeholders to perform needs analysis, develop learning objectives and design deliverables that meet the business needs and education services quality standards.
* Create and organize training sessions, and deliver instructor led training programs as needed to achieve business outcomes and effective transfer of learning in the workplace.
* Forge relationships with internal and external stakeholders to ensure that both tactical and strategic goals and outcomes are met.
* Complete hiring and performance management processes to support a high performing training team.
* Provide confidential coaching/facilitation and work closely with all levels of operations staff to ensure the integrity of the program and provide highly skilled feedback on staff performance in scheduled training and informal skills-based coaching.
* Ensure Training Specialists and Operations staff are provided with up-to-date knowledge of project related updates, processes, and procedures.
* Develop a means of measuring the effectiveness of training programs developed or administered through evaluation, testing, and assessment of program outcomes.
* Align content distribution paths with GitLab Partner ecosystems and experience to increase efficiency and scale
* Build a partner-centric approach for creating and updating content that answers the “what’s in it for the partner” question and resolves what’s missing from a partner perspective when delivering customer education
* Manage training delivery and content development of partner and customer education programs
* Design and implement new training and certification programs to our Partner community, including virtual and onsite instructor-led training, webinars, and e-learning
* Design and manage a feedback program to evaluate training content, establish KPIs, and maintain a continuous improvement cycle
* Work with our internal operations team to manage the Education Services technical environments and LMS platforms
* Collaborate cross-functionally with internal teams in Professional Services, Product Management, Marketing, and Sales
* Manage relationships with Partner Certified Trainers and community of practice
Collaborate with Senior leadership to ensure financial and operational goals are met

#### Senior Manager, Education Delivery Requirements

* Bachelor’s degree in business, management, education or related field.
* 5+ years building and managing for-profit education businesses, including product management, learning content delivery, training delivery, and certification programs.
* Comprehensive knowledge of the principles, methods, and techniques used in the development and delivery of training and certification programs
* Comprehensive knowledge of relevant training technologies, such as Learning Management Systems (LMS).
* Excellent written and verbal communication skills with the ability to focus and clarify concepts
* Demonstrated problem solving and decision-making abilities with effective organizational and time management skills; the ability to handle multiple projects and priorities effectively in a fast-paced environment with minimal supervision
* Strong organizational, multi-tasking and presentation skills. Ability to create momentum and foster organizational change
* Must exhibit initiative, decisiveness and creativity, along with self-motivation and the ability to assume responsibility and maintain strict confidentiality
* Proven ability to conduct training needs assessments with key stakeholders.
* Proven ability to develop training curriculum and deliver effective and satisfying learning experiences.
* Experience with performance management, coaching, and mentoring team members. 
- You share our [values](/handbook/values/), and work in accordance with those values.
- Ability to use GitLab

## Performance Indicators

* [Bookings attached rate per agreed plan](/handbook/sales/#pcv)
* [Services bookings and revenue per agreed plan](/handbook/sales/#pcv)

## Career Ladder

The next steps for the Professional Services Education Family would be to move to the [Director, Professional Services](/job-families/sales/director-of-professional-services/) Job family.

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

- Selected candidates will be invited to schedule a 30min [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters
- Next, candidates will be invited to schedule a first interview with the Hiring Manager
- Next, candidates will be invited to interview with 2-5 team members
- There may be a final executive interview 

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
